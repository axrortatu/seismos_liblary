package seismos.uz.payload;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import seismos.uz.entity.template.AbsEntity;

@AllArgsConstructor
@NoArgsConstructor
@Data

public class ReqSource {

    private String name;
    private String id_number;
    private Integer quantity;
    private Integer created_date;
}
