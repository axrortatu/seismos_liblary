package seismos.uz.payload;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data

public class ReqTakenSource {
    private String fullName;
    private String soureName;
    private String id_number;
}
