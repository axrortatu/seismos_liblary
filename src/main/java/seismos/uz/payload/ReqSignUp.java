package seismos.uz.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import seismos.uz.utils.PhoneNumberValidation;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.UUID;

@Data
@AllArgsConstructor

public class ReqSignUp {

    @NotNull
    @Size(min = 3, max = 50)
    private String firstName;

    @NotNull
    @Size(min = 3, max = 50)
    private String lastName;

    @NotNull
    @Size(min = 3, max = 50)
    private String username;

    @NotNull
    @Size(min = 3, max = 50)
    private String password;

    @PhoneNumberValidation
    private String phoneNumber;

}
