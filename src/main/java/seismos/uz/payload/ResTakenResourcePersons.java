package seismos.uz.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data

public class ResTakenResourcePersons {
    private String personFirstName;
    private String personLastName;
    private String username;
    private String recourceName;
}
