package seismos.uz.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ReqChangePassword {

    private String oldPassword;
    private String password;
    private String rePassword;

}
