package seismos.uz.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data

public class ReqUploadFile {

    private  UUID uuid;
    private  String fileName;
    private  String fileDownloadUrl;
    private  String fileContentType;
    private  long size;

}
