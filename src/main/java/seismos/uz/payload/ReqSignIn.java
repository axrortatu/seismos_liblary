package seismos.uz.payload;



import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Data

public class ReqSignIn {
    @NotNull
    private String username;
    @NotNull
    private String password;
}
