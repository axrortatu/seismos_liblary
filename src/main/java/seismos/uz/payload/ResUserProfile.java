package seismos.uz.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data

public class ResUserProfile {

    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String avatarUrl;
}
