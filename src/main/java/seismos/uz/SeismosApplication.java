package seismos.uz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SeismosApplication {
    public static void main(String[] args) {
        SpringApplication.run(SeismosApplication.class, args);
    }

}
