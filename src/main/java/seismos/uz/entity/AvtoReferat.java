package seismos.uz.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import seismos.uz.entity.template.AbsEntity;

import javax.persistence.*;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity

public class AvtoReferat extends AbsEntity {
    @Column(nullable = false)
    private String name;

    @Column(unique = true)
    private String id_number;

    @Column(nullable = false)
    private Integer quantity;

    @Column(nullable = true)
    private Integer created_date;

    @OneToOne(fetch = FetchType.LAZY)
    Attachment attachment;


}
