package seismos.uz.entity;

import lombok.EqualsAndHashCode;
import seismos.uz.entity.template.AbsEntityUsersTaken;

import javax.persistence.Entity;

@EqualsAndHashCode(callSuper = true)
@Entity

public class TakenReports extends AbsEntityUsersTaken {
}
