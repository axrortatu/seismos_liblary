package seismos.uz.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import seismos.uz.entity.template.AbsEntity;
import seismos.uz.entity.template.AbsEntityUsersTaken;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Entity

public class TakenJournals extends AbsEntityUsersTaken {
}
