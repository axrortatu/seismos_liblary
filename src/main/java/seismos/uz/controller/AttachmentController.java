package seismos.uz.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import seismos.uz.service.AttachmentSercive;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

@Controller
@RequestMapping("/api/file")
public class AttachmentController {

    @Autowired
    AttachmentSercive attachmentSercive;

    @PostMapping
    public HttpEntity<?> uploadFile(@RequestParam("file") MultipartFile file) throws IOException {
     return ResponseEntity.status(HttpStatus.CREATED).body(attachmentSercive.uploadFile(file));
    }

    @GetMapping("/{id}")
    public HttpEntity<?> getFile(@PathVariable UUID id, HttpServletResponse response) {
        return attachmentSercive.getAttachmentContent(id, response);
    }

}
