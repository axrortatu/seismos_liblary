package seismos.uz.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import seismos.uz.service.CountService;

@Controller
@RequestMapping(value = "/api")
public class CountController {

    @Autowired
    CountService countService;

    @GetMapping(value = "/taken/books/count")
    public HttpEntity<?> getBooksCount(){
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(countService.getBooks());
    }

//    @GetMapping(value = "/taken/disertationss/count")
//    public HttpEntity<?> getDisertationCount(){
//        return ResponseEntity.status(HttpStatus.ACCEPTED).body(countService.getDisertationCount());
//    }
//
//    @GetMapping(value = "/taken/reports/count")
//    public HttpEntity<?> getReportsCount(){
//        return ResponseEntity.status(HttpStatus.ACCEPTED).body(countService.getReportCount());
//    }
//
//    @GetMapping(value = "/taken/avtoreferats/count")
//    public HttpEntity<?> getAvtoReferatsCount(){
//        return ResponseEntity.status(HttpStatus.ACCEPTED).body(countService.getAvtoReferatCount());
//    }
//
//    @GetMapping(value = "/taken/journals/count")
//    public HttpEntity<?> getJournalsCount(){
//        return ResponseEntity.status(HttpStatus.ACCEPTED).body(countService.getJournalCount());
//    }


}
