//package seismos.uz.controller;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpEntity;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.*;
//import org.springframework.web.multipart.MultipartFile;
//import seismos.uz.entity.User;
//import seismos.uz.payload.ApiResponse;
//import seismos.uz.payload.ReqChangeName;
//import seismos.uz.payload.ReqChangePassword;
//import seismos.uz.repository.UserRepository;
//import seismos.uz.security.CurrentUser;
//import seismos.uz.service.UserService;
//
//import javax.validation.Valid;
//import java.io.IOException;
//
//@Controller
//@RequestMapping(value = "api/user")
//public class UserController {
//
//    @Autowired
//    UserService userService;
//    @Autowired
//    UserRepository userRepository;
//
//    @PostMapping(value = "/change/password")
//    public HttpEntity<?> changePassword(@Valid @RequestBody ReqChangePassword reqChangePassword, @CurrentUser User user ){
//        ApiResponse apiResponse = userService.changePassword(reqChangePassword, user);
//        return ResponseEntity.status(apiResponse.isSuccess() ? HttpStatus.ACCEPTED : HttpStatus.CONFLICT).body(apiResponse);
//    }
//
//    @PostMapping(value = "/change/firstNameAndlastName")
//    public HttpEntity<?> changeFirstNameAndLastName(@Valid @RequestBody ReqChangeName reqChangeName , @CurrentUser User user){
//        ApiResponse apiResponse = userService.changeFirstNameAndLastName(reqChangeName, user);
//        return ResponseEntity.status(apiResponse.isSuccess()? HttpStatus.ACCEPTED:HttpStatus.CONFLICT).body(apiResponse);
//    }
//
//    @PostMapping("/change/avatar")
//    public HttpEntity<?> changeAvatar(@RequestParam("file") MultipartFile file, @CurrentUser User user) throws IOException {
//        return ResponseEntity.status(HttpStatus.ACCEPTED).body(userService.changeAvatar(file, user));
//    }
//
//    @GetMapping("profile")
//    public HttpEntity<?> getUserProfile(@CurrentUser User user){
//        return ResponseEntity.status(HttpStatus.ACCEPTED).body(userService.getUserProfile(user));
//    }
//
//
//    @GetMapping("allUser")
//    public HttpEntity<?> getAllUserProfile(){
//        return ResponseEntity.status(HttpStatus.ACCEPTED).body(userRepository.findAll());
//    }
//
//}