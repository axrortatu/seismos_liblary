package seismos.uz.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import seismos.uz.entity.*;
import seismos.uz.payload.ApiResponse;
import seismos.uz.payload.ReqTakenSource;
import seismos.uz.repository.*;
import seismos.uz.service.ActionService;

import javax.validation.Valid;
import java.util.UUID;

@Controller
@RequestMapping("api/taken")
public class TakenSourceController {
    private final TakenBooksRepository takenBooksRepository;
    private final TakenReportsRepository takenReportsRepository;
    private final TakenAvtoReferatsRepository takenAvtoReferatsRepository;
    private final TakenJournalsRepository takenJournalsRepository;
    private final TakenDisertationsRepository takenDisertationsRepository;
    private final ActionService actionService;

    @Autowired
    public TakenSourceController(TakenBooksRepository takenBooksRepository, TakenReportsRepository takenReportsRepository, TakenAvtoReferatsRepository takenAvtoReferatsRepository, TakenJournalsRepository takenJournalsRepository, TakenDisertationsRepository takenDisertationsRepository, ActionService actionService) {
        this.takenBooksRepository = takenBooksRepository;
        this.takenReportsRepository = takenReportsRepository;
        this.takenAvtoReferatsRepository = takenAvtoReferatsRepository;
        this.takenJournalsRepository = takenJournalsRepository;
        this.takenDisertationsRepository = takenDisertationsRepository;
        this.actionService = actionService;
    }


    @GetMapping("taken_books")
    public HttpEntity<?> getBooks(
            @RequestParam(name = "page", required = false, defaultValue = "1") int page,
            @RequestParam(name = "term", required = false, defaultValue = "")  String term
    ){
        Page<TakenBooks> bookPage;
        page = (page<1)?1:page;

        PageRequest pageRequest = PageRequest.of(--page, 20, Sort.by(Sort.Direction.DESC, "id"));
        if (term.isEmpty()) {
            bookPage = takenBooksRepository.findAll(pageRequest);
        } else {
            bookPage = takenBooksRepository.findByFullName(term, pageRequest);
        }
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(bookPage);
    }

    @GetMapping("taken_dissertations")
    public HttpEntity<?> getDisertations(
            @RequestParam(name = "page", required = false, defaultValue = "1") int page,
            @RequestParam(name = "term", required = false, defaultValue = "")  String term
    ){
        Page<TakenDisertations> bookPage;
        page = (page<1)?1:page;

        PageRequest pageRequest = PageRequest.of(--page, 20, Sort.by(Sort.Direction.DESC, "id"));
        if (term.isEmpty()) {
            bookPage = takenDisertationsRepository.findAll(pageRequest);
        } else {
            bookPage = takenDisertationsRepository.findByFullName(term, pageRequest);
        }
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(bookPage);
    }


    @GetMapping("taken_avtoreferats")
    public HttpEntity<?> getAvtoReferats(
            @RequestParam(name = "page", required = false, defaultValue = "1") int page,
            @RequestParam(name = "term", required = false, defaultValue = "")  String term
    ){
        Page<TakenAvtoReferats> bookPage;
        page = (page<1)?1:page;

        PageRequest pageRequest = PageRequest.of(--page, 20, Sort.by(Sort.Direction.DESC, "id"));
        if (term.isEmpty()) {
            bookPage = takenAvtoReferatsRepository.findAll(pageRequest);
        } else {
            bookPage = takenAvtoReferatsRepository.findByFullName(term, pageRequest);
        }
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(bookPage);
    }


    @GetMapping("taken_journals")
    public HttpEntity<?> getJournals(
            @RequestParam(name = "page", required = false, defaultValue = "1") int page,
            @RequestParam(name = "term", required = false, defaultValue = "")  String term
    ){
        Page<TakenJournals> bookPage;
        page = (page<1)?1:page;

        PageRequest pageRequest = PageRequest.of(--page, 20, Sort.by(Sort.Direction.DESC, "id"));
        if (term.isEmpty()) {
            bookPage = takenJournalsRepository.findAll(pageRequest);
        } else {
            bookPage = takenJournalsRepository.findByFullName(term, pageRequest);
        }
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(bookPage);
    }

    @GetMapping("taken_reports")
    public HttpEntity<?> getReports(
            @RequestParam(name = "page", required = false, defaultValue = "1") int page,
            @RequestParam(name = "term", required = false, defaultValue = "")  String term
    ){
        Page<TakenReports> bookPage;
        page = (page<1)?1:page;

        PageRequest pageRequest = PageRequest.of(--page, 20, Sort.by(Sort.Direction.DESC, "id"));
        if (term.isEmpty()) {
            bookPage = takenReportsRepository.findAll(pageRequest);
        } else {
            bookPage = takenReportsRepository.findByFullName(term, pageRequest);
        }
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(bookPage);
    }

    @DeleteMapping("delete/taken_books/{id}")
    public HttpEntity<?> deleteTakenBook(@PathVariable UUID id){
        ApiResponse response = actionService.deleteTakenBook(id);
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(response);
    }

    @DeleteMapping("delete/taken_dissertations/{id}")
    public HttpEntity<?> deleteTakenDisertation(@PathVariable UUID id){
        ApiResponse response = actionService.deleteTakenDisertation(id);
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(response);
    }

    @DeleteMapping("delete/taken_journals/{id}")
    public HttpEntity<?> deleteTakenJournal(@PathVariable UUID id){
        ApiResponse response = actionService.deleteTakenJournal(id);
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(response);
    }

    @DeleteMapping("delete/taken_avtoreferats/{id}")
    public HttpEntity<?> deleteTakenAvtoreferat(@PathVariable UUID id){
        ApiResponse response = actionService.deleteTakenAvtoReferat(id);
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(response);
    }
    @DeleteMapping("delete/taken_reports/{id}")
    public HttpEntity<?> deleteTakenReport(@PathVariable UUID id){
        ApiResponse response = actionService.deleteTakenReport(id);
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(response);
    }

    @GetMapping("getId/taken_books/{id}")
    public HttpEntity<?> getBookById(@PathVariable UUID id){
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(actionService.getTakenBooks(id));
    }

    @GetMapping("getId/taken_dissertations/{id}")
    public HttpEntity<?> getDisertationById(@PathVariable UUID id){
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(actionService.getTakenDisertations(id));
    }

    @GetMapping("getId/taken_reports/{id}")
    public HttpEntity<?> getReportById(@PathVariable UUID id){
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(actionService.getTakenReports(id));
    }

    @GetMapping("getId/taken_avtoreferats/{id}")
    public HttpEntity<?> getAvtoReferatById(@PathVariable UUID id){
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(actionService.getTakenAvtoreferrats(id));
    }

    @GetMapping("getId/taken_journals/{id}")
    public HttpEntity<?> getJournalById(@PathVariable UUID id) {
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(actionService.getTakenJournals(id));
    }

    @PutMapping("update/taken_journals/{id}")
    public HttpEntity<?>updateJournalById(@PathVariable UUID id, @Valid @RequestBody ReqTakenSource reqTakenSource){
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(actionService.updateTakenJournal(id,reqTakenSource));
    }

    @PutMapping("update/taken_reports/{id}")
    public HttpEntity<?>updateReportById(@PathVariable UUID id, @Valid @RequestBody ReqTakenSource reqTakenSource){
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(actionService.updateTakenReport(id,reqTakenSource));
    }

    @PutMapping("update/taken_dissertations/{id}")
    public HttpEntity<?>updateDisertationById(@PathVariable UUID id, @Valid @RequestBody ReqTakenSource reqTakenSource){
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(actionService.updateTakenDissertation(id,reqTakenSource));
    }

    @PutMapping("update/taken_books/{id}")
    public HttpEntity<?>updateBookById(@PathVariable UUID id, @Valid @RequestBody ReqTakenSource reqTakenSource){
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(actionService.updateTakenBook(id,reqTakenSource));
    }

    @PutMapping("update/taken_avtoreferats/{id}")
    public HttpEntity<?>updateAvtoReferatById(@PathVariable UUID id, @Valid @RequestBody ReqTakenSource reqTakenSource){
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(actionService.updateTakenAvtoreferat(id,reqTakenSource));
    }

}
