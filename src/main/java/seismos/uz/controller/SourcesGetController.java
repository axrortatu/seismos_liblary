package seismos.uz.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import seismos.uz.entity.*;
import seismos.uz.payload.ApiResponse;
import seismos.uz.payload.ReqSource;
import seismos.uz.repository.*;
import seismos.uz.service.ActionService;
import seismos.uz.service.SoureSecureController;

import javax.validation.Valid;
import java.util.Optional;
import java.util.UUID;

@Controller
@RequestMapping("api/")
public class SourcesGetController {

    @Autowired
    SoureSecureController soureSecureController;

    private BookRepository bookRepository;
    private JournalRepository journalRepository;
    private ReportRepository reportRepository;
    private DisertationRepository disertationRepository;
    private AvtoReferatRepository avtoReferatRepository;
    private ActionService actionService;
    private UserRepository userRepository;

    @Autowired
    public SourcesGetController(BookRepository bookRepository, JournalRepository journalRepository, ReportRepository reportRepository, DisertationRepository disertationRepository, AvtoReferatRepository avtoReferatRepository, ActionService actionService, UserRepository userRepository) {
        this.bookRepository = bookRepository;
        this.journalRepository = journalRepository;
        this.reportRepository = reportRepository;
        this.disertationRepository = disertationRepository;
        this.avtoReferatRepository = avtoReferatRepository;
        this.actionService = actionService;
        this.userRepository = userRepository;
    }

    @Value("${pageable.size}")
    private Integer sizePage;


    @PostMapping("save/soure")
    public HttpEntity<?> saveBooks(@Valid @RequestBody ReqSource reqSource, @RequestParam(name = "id") UUID id, @RequestParam(name = "controller") String controller){
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(soureSecureController.saveSources(reqSource,id,controller));
    }

    @GetMapping("get/books")
    public HttpEntity<?> getBooks(
                @RequestParam(name = "page", required = false, defaultValue = "1") int page,
                @RequestParam(name = "term", required = false, defaultValue = "")  String term
            ){
        Page<Book> bookPage;
        page = (page<1)?1:page;

        PageRequest pageRequest = PageRequest.of(--page, sizePage, Sort.by(Sort.Direction.DESC, "id"));
        if (term.isEmpty()) {
             bookPage = bookRepository.findAll(pageRequest);
        } else {
             bookPage = bookRepository.findByNameOrCreatedDate(term, pageRequest);
        }
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(bookPage);
    }



    @GetMapping("get/journals")
    public HttpEntity<?> getJournals(
            @RequestParam(name = "page", required = false, defaultValue = "1") int page,
            @RequestParam(name = "term", required = false, defaultValue = "")  String term
    ){
        Page<Journal> bookPage;
        page = (page<1)?1:page;

        PageRequest pageRequest = PageRequest.of(--page, sizePage, Sort.by(Sort.Direction.DESC, "id"));
        if (term == null || term.isEmpty()) {
            bookPage = journalRepository.findAll(pageRequest);
        } else {
            bookPage = journalRepository.findByName(term, pageRequest);
        }

        return ResponseEntity.status(HttpStatus.ACCEPTED).body(bookPage);
    }


    @GetMapping("get/reports")
    public HttpEntity<?> getReports(
            @RequestParam(name = "page", required = false, defaultValue = "1") int page,
            @RequestParam(name = "term", required = false, defaultValue = "")  String term
    ){

        Page<Report> bookPage;
        page = (page<1)?1:page;

        PageRequest pageRequest = PageRequest.of(--page, sizePage, Sort.by(Sort.Direction.DESC, "id"));
        if (term == null || term.isEmpty()) {
            bookPage = reportRepository.findAll(pageRequest);
        } else {
            bookPage = reportRepository.findByName(term, pageRequest);
        }

        return ResponseEntity.status(HttpStatus.ACCEPTED).body(bookPage);
    }


    @GetMapping("get/disertations")
    public HttpEntity<?> getDisertations(
            @RequestParam(name = "page", required = false, defaultValue = "1") int page,
            @RequestParam(name = "term", required = false, defaultValue = "")  String term
    ){
        Page<Disertation> bookPage;
        page = (page<1)?1:page;

        PageRequest pageRequest = PageRequest.of(--page, sizePage, Sort.by(Sort.Direction.DESC, "id"));
        if (term == null || term.isEmpty()) {
            bookPage = disertationRepository.findAll(pageRequest);
        } else {
            bookPage = disertationRepository.findByName(term, pageRequest);
        }

        return ResponseEntity.status(HttpStatus.ACCEPTED).body(bookPage);
    }


    @GetMapping("get/avtoreferats")
    public HttpEntity<?> getAvtoReferats(
            @RequestParam(name = "page", required = false, defaultValue = "1") int page,
            @RequestParam(name = "term", required = false, defaultValue = "")  String term
    ){
        Page<AvtoReferat> bookPage;
        page = (page<1)?1:page;

        PageRequest pageRequest = PageRequest.of(--page, sizePage, Sort.by(Sort.Direction.DESC, "id"));
        if (term == null || term.isEmpty()) {
            bookPage = avtoReferatRepository.findAll(pageRequest);
        } else {
            bookPage = avtoReferatRepository.findByName(term, pageRequest);
        }

        return ResponseEntity.status(HttpStatus.ACCEPTED).body(bookPage);

    }

    @GetMapping("get/users")
    public HttpEntity<?> getUsers(){
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(userRepository.findAll());
    }

    /* --- begin: delete Resource By ID*/
    @DeleteMapping("delete/book/{id}")
    public HttpEntity<?> deleteBook(@PathVariable UUID id){
        ApiResponse response = actionService.deleteBook(id);
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(response);
    }

    @DeleteMapping("delete/disertation/{id}")
    public HttpEntity<?> deleteDisertation(@PathVariable UUID id){
        ApiResponse response = actionService.deleteDisertation(id);
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(response);
    }
    @DeleteMapping("delete/journal/{id}")
    public HttpEntity<?> deleteJournal(@PathVariable UUID id){
        ApiResponse response = actionService.deleteJournals(id);
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(response);
    }
    @DeleteMapping("delete/avtoreferat/{id}")
    public HttpEntity<?> deleteAvtoreferat(@PathVariable UUID id){
        ApiResponse response = actionService.deleteAvtoreferat(id);
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(response);
    }
    @DeleteMapping("delete/report/{id}")
    public HttpEntity<?> deleteReport(@PathVariable UUID id){
        ApiResponse response = actionService.deleteReport(id);
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(response);
    }
    /* --- end: delete Resource By ID*/


    /* --- begin: get Resource By ID*/
    @GetMapping("getId/book/{id}")
    public HttpEntity<?> getBookById(@PathVariable UUID id){
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(actionService.getBook(id));
    }

    @GetMapping("getId/disertation/{id}")
    public HttpEntity<?> getDisertationById(@PathVariable UUID id){
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(actionService.getDisertation(id));
    }

    @GetMapping("getId/report/{id}")
    public HttpEntity<?> getReportById(@PathVariable UUID id){
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(actionService.getReport(id));
    }

    @GetMapping("getId/avtoreferat/{id}")
    public HttpEntity<?> getAvtoReferatById(@PathVariable UUID id){
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(actionService.getAvtoReferat(id));
    }

    @GetMapping("getId/journal/{id}")
    public HttpEntity<?> getJournalById(@PathVariable UUID id){
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(actionService.getJournal(id));
    }
    /* --- end: get Resource By ID*/


    @PutMapping("update/book/{id}")
    public HttpEntity<?> updatdeBook(@PathVariable UUID id, @Valid @RequestBody ReqSource reqSource){
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(actionService.updateBook(id,reqSource));
    }

    @PutMapping("update/disertation/{id}")
    public HttpEntity<?> updatdeDisertation(@PathVariable UUID id, @Valid @RequestBody ReqSource reqSource){
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(actionService.updateDisertation(id,reqSource));
    }

    @PutMapping("update/report/{id}")
    public HttpEntity<?> updatdeReport(@PathVariable UUID id, @Valid @RequestBody ReqSource reqSource){
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(actionService.updateReport(id,reqSource));
    }

    @PutMapping("update/avtoreferat/{id}")
    public HttpEntity<?> updatdeAvtoReferat(@PathVariable UUID id, @Valid @RequestBody ReqSource reqSource){
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(actionService.updateAvtoreferat(id,reqSource));
    }

    @PutMapping("update/journal/{id}")
    public HttpEntity<?> updatdeJournal(@PathVariable UUID id, @Valid @RequestBody ReqSource reqSource){
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(actionService.updateJournal(id,reqSource));
    }


}
