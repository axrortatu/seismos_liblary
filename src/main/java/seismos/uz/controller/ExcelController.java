package seismos.uz.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import seismos.uz.payload.ReqExcel;
import seismos.uz.service.ExcelGenerator;

import java.io.ByteArrayInputStream;
import java.io.IOException;

@Controller
@RequestMapping(value = "api/excel")
public class ExcelController {

    private final ExcelGenerator excelGenerator;

    @Autowired
    public ExcelController(ExcelGenerator excelGenerator) {
        this.excelGenerator = excelGenerator;
    }

    @GetMapping("/soure")
    public ResponseEntity<?> excelCustomersReport(@RequestParam (name = "begin") Integer begin, @RequestParam(name = "end") Integer end, @RequestParam(name = "check") String check) throws IOException {
        ByteArrayInputStream in = excelGenerator.customersToExcel(begin,end, check);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "attachment; filename=customers.xlsx");

        return ResponseEntity
                .ok()
                .headers(headers)
                .body(new InputStreamResource(in));
    }


}
