//package seismos.uz.security;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.core.userdetails.UsernameNotFoundException;
//import org.springframework.security.crypto.password.PasswordEncoder;
//import org.springframework.stereotype.Service;
//import seismos.uz.entity.User;
//import seismos.uz.enums.RoleName;
//import seismos.uz.payload.ApiResponse;
//import seismos.uz.payload.ReqSignUp;
//import seismos.uz.repository.RoleRepository;
//import seismos.uz.repository.UserRepository;
//
//import java.util.UUID;
//
//@Service
//public class AuthService implements UserDetailsService {
//
//    @Autowired
//    UserRepository userRepository;
//
//    @Autowired
//    PasswordEncoder passwordEncoder;
//
//    @Autowired
//    RoleRepository roleRepository;
//
//
//    @Override
//    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//        User user = userRepository.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException("this user name is not found" + username));
//        return user;
//    }
//
//    public UserDetails loadUseryId(UUID uuid){
//        return userRepository.findById(uuid).orElseThrow(() -> new UsernameNotFoundException("this user is not found"));
//    }
//
//    public ApiResponse register(ReqSignUp reqSignUp){
//        if(userRepository.findByUsername(reqSignUp.getUsername()).isPresent()){
//            return new ApiResponse("bu user mavjud ", false);
//        }
//        else {
//            userRepository.save(
//                    new User(reqSignUp.getFirstName(),
//                            reqSignUp.getLastName(),
//                            reqSignUp.getUsername(),
//                            passwordEncoder.encode(reqSignUp.getPassword()), reqSignUp.getPhoneNumber(), roleRepository.findAllByRoleName(RoleName.ROLE_USER)));
//            return new ApiResponse("Tizimga muvofaqiyatli saqlandingiz", true);
//        }
//    }
//}
