package seismos.uz.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import seismos.uz.entity.*;
import seismos.uz.payload.ApiResponse;
import seismos.uz.payload.ReqSource;
import seismos.uz.repository.*;

import java.util.Optional;
import java.util.UUID;

@Service
public class SoureSecureController {

    private final BookRepository bookRepository;
    private final DisertationRepository disertationRepository;
    private final AvtoReferatRepository avtoReferatRepository;
    private final ReportRepository reportRepository;
    private final JournalRepository journalRepository;
    private final AttachmenntRepository attachmenntRepository;

    @Autowired
    public SoureSecureController(BookRepository bookRepository, DisertationRepository disertationRepository, AvtoReferatRepository avtoReferatRepository, ReportRepository reportRepository, JournalRepository journalRepository, AttachmenntRepository attachmenntRepository) {
        this.bookRepository = bookRepository;
        this.disertationRepository = disertationRepository;
        this.avtoReferatRepository = avtoReferatRepository;
        this.reportRepository = reportRepository;
        this.journalRepository = journalRepository;
        this.attachmenntRepository = attachmenntRepository;
    }

    public ApiResponse saveSources(ReqSource reqSource, UUID id, String controller){
        ApiResponse apiResponse = new ApiResponse();
        Optional<Attachment> attachment = attachmenntRepository.findById(id);
        switch (controller){
            case "book":  bookRepository.save(new Book(reqSource.getName(),reqSource.getId_number(),reqSource.getQuantity(),reqSource.getCreated_date(), attachment.get())); apiResponse.setSuccess(true); apiResponse.setMessage("Successfully saved"); break;
            case "journal": journalRepository.save(new Journal(reqSource.getName(),reqSource.getId_number(),reqSource.getQuantity(),reqSource.getCreated_date(), attachment.get())); apiResponse.setSuccess(true); apiResponse.setMessage("Successfully saved"); break;
            case "avtoreferat": avtoReferatRepository.save(new AvtoReferat(reqSource.getName(),reqSource.getId_number(),reqSource.getQuantity(),reqSource.getCreated_date(), attachment.get())); apiResponse.setSuccess(true); apiResponse.setMessage("Successfully saved"); break;
            case "disertation": disertationRepository.save(new Disertation(reqSource.getName(),reqSource.getId_number(),reqSource.getQuantity(),reqSource.getCreated_date(), attachment.get())); apiResponse.setSuccess(true); apiResponse.setMessage("Successfully saved"); break;
            case "report": reportRepository.save(new Report(reqSource.getName(),reqSource.getId_number(),reqSource.getQuantity(),reqSource.getCreated_date(), attachment.get())); apiResponse.setSuccess(true); apiResponse.setMessage("Successfully saved"); break;
            default:apiResponse.setSuccess(false); apiResponse.setMessage("Saqlanmadi"); break;
        }
        return apiResponse;
    }




}
