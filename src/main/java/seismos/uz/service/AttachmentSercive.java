package seismos.uz.service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import seismos.uz.entity.Attachment;
import seismos.uz.entity.AttachmentContent;
import seismos.uz.exception.ResourceNotFoundException;
import seismos.uz.payload.ReqUploadFile;
import seismos.uz.repository.AttachmenntRepository;
import seismos.uz.repository.AttachmentContentRepository;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

@Service
public class AttachmentSercive {

    @Autowired
    AttachmenntRepository attachmenntRepository;

    @Autowired
    AttachmentContentRepository attachmentContentRepository;

    public ReqUploadFile uploadFile(MultipartFile file) throws IOException {
        Attachment attachment = attachmenntRepository.save(new Attachment(file.getOriginalFilename(), file.getContentType(), file.getSize()));
        AttachmentContent attachmentContent = attachmentContentRepository.save(new AttachmentContent(attachment, file.getBytes()));

        return new ReqUploadFile(
                attachment.getId(),
                attachment.getName(),
                ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/file/").path(attachment.getId().toString()).toUriString(),
                attachment.getContentType(),
                attachment.getSize()
                );
    }
    public HttpEntity<?> getAttachmentContent(UUID attachmentId, HttpServletResponse response){
        Attachment attachment = attachmenntRepository.findById(attachmentId).orElseThrow(() -> new ResourceNotFoundException("Attachment", "id", attachmentId));
        AttachmentContent attachmentContent = attachmentContentRepository.findByAttachment(attachment).orElseThrow(() -> new ResourceNotFoundException("Attachment content", "attachment id", attachmentId));
        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(attachment.getContentType()))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + attachment.getName() + "\"")
                .body(attachmentContent.getContent());
    }
}
