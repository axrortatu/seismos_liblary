package seismos.uz.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import seismos.uz.entity.*;
import seismos.uz.payload.ApiResponse;
import seismos.uz.payload.ReqSource;
import seismos.uz.payload.ReqTakenSource;
import seismos.uz.repository.*;

import java.util.Optional;
import java.util.UUID;


@Service
public class ActionService {

    private boolean checkController;
    private String name;

    public boolean isCheckController() {
        return checkController;
    }

    public void setCheckController(boolean checkController) {
        this.checkController = checkController;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private BookRepository bookRepository;
    private JournalRepository journalRepository;
    private ReportRepository reportRepository;
    private DisertationRepository disertationRepository;
    private AvtoReferatRepository avtoReferatRepository;

    private final TakenBooksRepository takenBooksRepository;
    private final TakenReportsRepository takenReportsRepository;
    private final TakenAvtoReferatsRepository takenAvtoReferatsRepository;
    private final TakenJournalsRepository takenJournalsRepository;
    private final TakenDisertationsRepository takenDisertationsRepository;

    @Autowired
    public ActionService(BookRepository bookRepository, JournalRepository journalRepository, ReportRepository reportRepository, DisertationRepository disertationRepository, AvtoReferatRepository avtoReferatRepository, TakenBooksRepository takenBooksRepository, TakenReportsRepository takenReportsRepository, TakenAvtoReferatsRepository takenAvtoReferatsRepository, TakenJournalsRepository takenJournalsRepository, TakenDisertationsRepository takenDisertationsRepository) {
        this.bookRepository = bookRepository;
        this.journalRepository = journalRepository;
        this.reportRepository = reportRepository;
        this.disertationRepository = disertationRepository;
        this.avtoReferatRepository = avtoReferatRepository;
        this.takenBooksRepository = takenBooksRepository;
        this.takenReportsRepository = takenReportsRepository;
        this.takenAvtoReferatsRepository = takenAvtoReferatsRepository;
        this.takenJournalsRepository = takenJournalsRepository;
        this.takenDisertationsRepository = takenDisertationsRepository;
    }


   /*  -------  delete  ---------*/
    public ApiResponse deleteDisertation(UUID uuid) {
        Optional<Disertation> source = disertationRepository.findById(uuid);
        if (source.isPresent()) {
            disertationRepository.deleteById(uuid);
            setName(source.get().getName());
            setCheckController(true);
            return baseAll();
        } else {
            setCheckController(false);
            return baseAll();
        }
    }

    public ApiResponse deleteBook(UUID uuid) {
        Optional<Book> source = bookRepository.findById(uuid);
        if (source.isPresent()) {
            bookRepository.deleteById(uuid);
            setName(source.get().getName());
            setCheckController(true);
            return baseAll();
        } else {
            setCheckController(false);
            return baseAll();
        }
    }

    public ApiResponse deleteReport(UUID uuid) {
        Optional<Report> source = reportRepository.findById(uuid);
        if (source.isPresent()) {
            reportRepository.deleteById(uuid);
            setName(source.get().getName());
            setCheckController(true);
            return baseAll();
        } else {
            setCheckController(false);
            return baseAll();
        }
    }

    public ApiResponse deleteAvtoreferat(UUID uuid) {
        Optional<AvtoReferat> source = avtoReferatRepository.findById(uuid);
        if (source.isPresent()) {
            avtoReferatRepository.deleteById(uuid);
            setName(source.get().getName());
            setCheckController(true);
            return baseAll();
        } else {
            setCheckController(false);
            return baseAll();
        }
    }

    public ApiResponse deleteJournals(UUID uuid) {
        Optional<Journal> source = journalRepository.findById(uuid);
        if (source.isPresent()) {
            journalRepository.deleteById(uuid);
            setName(source.get().getName());
            setCheckController(true);
            return baseAll();
        } else {
            setCheckController(false);
            return baseAll();
        }
    }

    public ApiResponse deleteTakenDisertation(UUID uuid) {
        Optional<TakenDisertations> source = takenDisertationsRepository.findById(uuid);
        if (source.isPresent()) {
            takenDisertationsRepository.deleteById(uuid);
            setName(source.get().getFullName());
            setCheckController(true);
            return baseAll();
        } else {
            setCheckController(false);
            return baseAll();
        }
    }

    public ApiResponse deleteTakenBook(UUID uuid) {
        Optional<TakenBooks> source = takenBooksRepository.findById(uuid);
        if (source.isPresent()) {
            takenBooksRepository.deleteById(uuid);
            setName(source.get().getFullName());
            setCheckController(true);
            return baseAll();
        } else {
            setCheckController(false);
            return baseAll();
        }
    }

    public ApiResponse deleteTakenAvtoReferat(UUID uuid) {
        Optional<TakenAvtoReferats> source = takenAvtoReferatsRepository.findById(uuid);
        if (source.isPresent()) {
            takenAvtoReferatsRepository.deleteById(uuid);
            setName(source.get().getFullName());
            setCheckController(true);
            return baseAll();
        } else {
            setCheckController(false);
            return baseAll();
        }
    }

    public ApiResponse deleteTakenReport(UUID uuid) {
        Optional<TakenReports> source = takenReportsRepository.findById(uuid);
        if (source.isPresent()) {
            takenReportsRepository.deleteById(uuid);
            setName(source.get().getFullName());
            setCheckController(true);
            return baseAll();
        } else {
            setCheckController(false);
            return baseAll();
        }
    }

    public ApiResponse deleteTakenJournal(UUID uuid) {
        Optional<TakenJournals> source = takenJournalsRepository.findById(uuid);
        if (source.isPresent()) {
            takenJournalsRepository.deleteById(uuid);
            setName(source.get().getFullName());
            setCheckController(true);
            return baseAll();
        } else {
            setCheckController(false);
            return baseAll();
        }
    }



    public ApiResponse baseAll(){
        ApiResponse response = new ApiResponse();
        if (checkController){
            response.setMessage(this.name + " muvafaqiyatli o'chirildi");
            response.setSuccess(true);
        } else {
            response.setMessage("Bunday ma'lumot ma'lumotlar bazasidan topilmadi");
            response.setSuccess(false);
        }
        return response;
    }

   /* ------    get By ID   -------*/
    public Book getBook(UUID uuid){
        Optional<Book> optionalBook = bookRepository.findById(uuid);
        if (optionalBook.isPresent()){
            return optionalBook.get();
        }else {
            return null;
        }
    }

    public Disertation getDisertation(UUID uuid){
        Optional<Disertation> optionalBook = disertationRepository.findById(uuid);
        if (optionalBook.isPresent()){
            return optionalBook.get();
        }else {
            return null;
        }
    }

    public AvtoReferat getAvtoReferat(UUID uuid){
        Optional<AvtoReferat> optionalBook = avtoReferatRepository.findById(uuid);
        if (optionalBook.isPresent()){
            return optionalBook.get();
        }else {
            return null;
        }
    }

    public Journal getJournal(UUID uuid){
        Optional<Journal> optionalBook = journalRepository.findById(uuid);
        if (optionalBook.isPresent()){
            return optionalBook.get();
        }else {
            return null;
        }
    }

    public Report getReport(UUID uuid){
        Optional<Report> optionalBook = reportRepository.findById(uuid);
        if (optionalBook.isPresent()){
            return optionalBook.get();
        }else {
            return null;
        }
    }


    public TakenBooks getTakenBooks(UUID uuid){
        Optional<TakenBooks> optionalBook = takenBooksRepository.findById(uuid);
        if (optionalBook.isPresent()){
            return optionalBook.get();
        }else {
            return null;
        }
    }

    public TakenDisertations getTakenDisertations(UUID uuid){
        Optional<TakenDisertations> optionalBook = takenDisertationsRepository.findById(uuid);
        if (optionalBook.isPresent()){
            return optionalBook.get();
        }else {
            return null;
        }
    }

    public TakenReports getTakenReports(UUID uuid){
        Optional<TakenReports> optionalBook = takenReportsRepository.findById(uuid);
        if (optionalBook.isPresent()){
            return optionalBook.get();
        }else {
            return null;
        }
    }

    public TakenJournals getTakenJournals(UUID uuid){
        Optional<TakenJournals> optionalBook = takenJournalsRepository.findById(uuid);
        if (optionalBook.isPresent()){
            return optionalBook.get();
        }else {
            return null;
        }
    }

    public TakenAvtoReferats getTakenAvtoreferrats(UUID uuid){
        Optional<TakenAvtoReferats> optionalBook = takenAvtoReferatsRepository.findById(uuid);
        if (optionalBook.isPresent()){
            return optionalBook.get();
        }else {
            return null;
        }
    }


    /*---      update   ----*/

    public ApiResponse updateBook(UUID uuid, ReqSource reqSource){
        Optional<Book> soure = bookRepository.findById(uuid);
        if (soure.isPresent()){
            Book book1 = soure.get();
            book1.setName(reqSource.getName());
            book1.setId_number(reqSource.getId_number());
            book1.setQuantity(reqSource.getQuantity());
            bookRepository.save(book1);
            setCheckController(true);
            setName(soure.get().getName());
            return baseUpdate();
        }
        else {
            setCheckController(false);
            return baseUpdate();
        }
    }

    public ApiResponse updateDisertation(UUID uuid, ReqSource reqSource){
        Optional<Disertation> soure = disertationRepository.findById(uuid);
        if (soure.isPresent()){
            Disertation book1 = soure.get();
            book1.setName(reqSource.getName());
            book1.setId_number(reqSource.getId_number());
            book1.setQuantity(reqSource.getQuantity());
            disertationRepository.save(book1);
            setCheckController(true);
            setName(soure.get().getName());
            return baseUpdate();
        }
        else {
            setCheckController(false);
            return baseUpdate();
        }
    }

    public ApiResponse updateReport(UUID uuid, ReqSource reqSource){
        Optional<Report> soure = reportRepository.findById(uuid);
        if (soure.isPresent()){
            Report book1 = soure.get();
            book1.setName(reqSource.getName());
            book1.setId_number(reqSource.getId_number());
            book1.setQuantity(reqSource.getQuantity());
            reportRepository.save(book1);
            setCheckController(true);
            setName(soure.get().getName());
            return baseUpdate();
        }
        else {
            setCheckController(false);
            return baseUpdate();
        }
    }

    public ApiResponse updateJournal(UUID uuid, ReqSource reqSource){
        Optional<Journal> soure = journalRepository.findById(uuid);
        if (soure.isPresent()){
            Journal book1 = soure.get();
            book1.setName(reqSource.getName());
            book1.setId_number(reqSource.getId_number());
            book1.setQuantity(reqSource.getQuantity());
            journalRepository.save(book1);
            setCheckController(true);
            setName(soure.get().getName());
            return baseUpdate();
        }
        else {
            setCheckController(false);
            return baseUpdate();
        }
    }

    public ApiResponse updateAvtoreferat(UUID uuid, ReqSource reqSource){
        Optional<AvtoReferat> soure = avtoReferatRepository.findById(uuid);
        if (soure.isPresent()){
            AvtoReferat book1 = soure.get();
            book1.setName(reqSource.getName());
            book1.setId_number(reqSource.getId_number());
            book1.setQuantity(reqSource.getQuantity());
            avtoReferatRepository.save(book1);
            setCheckController(true);
            setName(soure.get().getName());
            return baseUpdate();
        }
        else {
            setCheckController(false);
            return baseUpdate();
        }
    }


    public ApiResponse updateTakenAvtoreferat(UUID uuid, ReqTakenSource reqTakenSource){
        Optional<TakenAvtoReferats> soure = takenAvtoReferatsRepository.findById(uuid);
        if (soure.isPresent()){
            TakenAvtoReferats book1 = soure.get();
            book1.setFullName(reqTakenSource.getFullName());
            book1.setId_number(reqTakenSource.getId_number());
            book1.setId_number(reqTakenSource.getId_number());
            takenAvtoReferatsRepository.save(book1);
            setCheckController(true);
            setName(soure.get().getSoureName());
            return baseUpdate();
        }
        else {
            setCheckController(false);
            return baseUpdate();
        }
    }


    public ApiResponse updateTakenBook(UUID uuid, ReqTakenSource reqTakenSource){
        Optional<TakenBooks> soure = takenBooksRepository.findById(uuid);
        if (soure.isPresent()){
            TakenBooks book1 = soure.get();
            book1.setFullName(reqTakenSource.getFullName());
            book1.setId_number(reqTakenSource.getId_number());
            book1.setId_number(reqTakenSource.getId_number());
            takenBooksRepository.save(book1);
            setCheckController(true);
            setName(soure.get().getSoureName());
            return baseUpdate();
        }
        else {
            setCheckController(false);
            return baseUpdate();
        }
    }

    public ApiResponse updateTakenReport(UUID uuid, ReqTakenSource reqTakenSource){
        Optional<TakenReports> soure = takenReportsRepository.findById(uuid);
        if (soure.isPresent()){
            TakenReports book1 = soure.get();
            book1.setFullName(reqTakenSource.getFullName());
            book1.setId_number(reqTakenSource.getId_number());
            book1.setId_number(reqTakenSource.getId_number());
            takenReportsRepository.save(book1);
            setCheckController(true);
            setName(soure.get().getSoureName());
            return baseUpdate();
        }
        else {
            setCheckController(false);
            return baseUpdate();
        }
    }

    public ApiResponse updateTakenJournal(UUID uuid, ReqTakenSource reqTakenSource){
        Optional<TakenJournals> soure = takenJournalsRepository.findById(uuid);
        if (soure.isPresent()){
            TakenJournals book1 = soure.get();
            book1.setFullName(reqTakenSource.getFullName());
            book1.setId_number(reqTakenSource.getId_number());
            book1.setId_number(reqTakenSource.getId_number());
            takenJournalsRepository.save(book1);
            setCheckController(true);
            setName(soure.get().getSoureName());
            return baseUpdate();
        }
        else {
            setCheckController(false);
            return baseUpdate();
        }
    }

    public ApiResponse updateTakenDissertation(UUID uuid, ReqTakenSource reqTakenSource){
        Optional<TakenDisertations> soure = takenDisertationsRepository.findById(uuid);
        if (soure.isPresent()){
            TakenDisertations book1 = soure.get();
            book1.setFullName(reqTakenSource.getFullName());
            book1.setId_number(reqTakenSource.getId_number());
            book1.setId_number(reqTakenSource.getId_number());
            takenDisertationsRepository.save(book1);
            setCheckController(true);
            setName(soure.get().getSoureName());
            return baseUpdate();
        }
        else {
            setCheckController(false);
            return baseUpdate();
        }
    }




    public ApiResponse baseUpdate(){
        ApiResponse response = new ApiResponse();
        if (checkController){
            response.setMessage(this.name + " muvafaqiyatli almashtirildi");
            response.setSuccess(true);
        } else {
            response.setMessage("Bunday ma'lumot ma'lumotlar bazasidan topilmadi");
            response.setSuccess(false);
        }
        return response;
    }
}
