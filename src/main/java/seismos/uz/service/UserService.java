//package seismos.uz.service;
//
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.HttpEntity;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.security.core.userdetails.UsernameNotFoundException;
//import org.springframework.security.crypto.password.PasswordEncoder;
//import org.springframework.stereotype.Service;
//import org.springframework.web.multipart.MultipartFile;
//import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
//import seismos.uz.entity.*;
//import seismos.uz.exception.ResourceNotFoundException;
//import seismos.uz.payload.*;
//import seismos.uz.repository.AttachmenntRepository;
//import seismos.uz.repository.TakenAvtoReferatsRepository;
//import seismos.uz.repository.TakenBooksRepository;
//import seismos.uz.repository.UserRepository;
//
//import java.io.IOException;
//
//@Service
//public class UserService {
//
//    @Autowired
//    UserRepository  userRepository;
//
//    @Autowired
//    PasswordEncoder passwordEncoder;
//
//    @Autowired
//    AttachmentSercive attachmentSercive;
//
//    @Autowired
//    AttachmenntRepository attachmenntRepository;
//
//
//
//
//    public ApiResponse changePassword(ReqChangePassword reqChangePassword, User currentUser){
//
//        User user = userRepository.findById(currentUser.getId()).orElseThrow(() -> new UsernameNotFoundException("Bunday User tizimda mavjud emas"));
//        if (passwordEncoder.matches(reqChangePassword.getOldPassword(),user.getPassword())){
//            user.setPassword(passwordEncoder.encode(reqChangePassword.getPassword()));
//            userRepository.save(user);
//            return new ApiResponse("Parolingiz muvafaqiyatli almashtirildi", true);
//        }else {
//            return new ApiResponse("Amamldagi parol natog'ri kiritildi",false);
//        }
//    }
//
//    public ApiResponse changeFirstNameAndLastName(ReqChangeName reqChangeName, User user){
//        User user1 = userRepository.findById(user.getId()).orElseThrow(() -> new UsernameNotFoundException("Bunday User tizimda mavjud emas"));
//        if (passwordEncoder.matches(reqChangeName.getPassword(),user1.getPassword())){
//            user1.setFirstName(reqChangeName.getFirstName());
//            user1.setLastName(reqChangeName.getLastName());
//            userRepository.save(user1);
//            return new ApiResponse("Ismingiz va Familiyangiz muvofaqiyatli almashtirildi",true);
//        }else {
//            return new ApiResponse("Amamldagi parol natog'ri kiritildi",false);
//        }
//    }
//
//    public HttpEntity<?> changeAvatar(MultipartFile file, User currentUser)throws IOException {
//        if (file.getContentType().toLowerCase().equals("image/jpg")
//                || file.getContentType().toLowerCase().equals("image/jpeg")
//                || file.getContentType().toLowerCase().equals("image/png")){
//
//            ReqUploadFile uploadFile = attachmentSercive.uploadFile(file);
//            User user = userRepository.findById(currentUser.getId()).orElseThrow(() -> new UsernameNotFoundException("Bunday foydalanuvchi topilmadi!"));
//            Attachment attachment = attachmenntRepository.findById(uploadFile.getUuid()).orElseThrow(() -> new ResourceNotFoundException("File", "id", uploadFile.getUuid()));
//            user.setPhoto(attachment);
//            userRepository.save(user);
//            return ResponseEntity.status(HttpStatus.ACCEPTED).body(uploadFile);
//        }else{
//            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ApiResponse("Foydalanuvchi rasm formati no'to'g'ri. Iltimos, *.png, *.jpg rasmlar tanlang!", false));
//        }
//    }
//
//    public ResUserProfile getUserProfile(User user){
//
//        ResUserProfile userProfile = new ResUserProfile();
//        userProfile.setFirstName(user.getFirstName());
//        userProfile.setLastName(user.getLastName());
//        userProfile.setPhoneNumber(user.getPhoneNumber());
//        if (user.getPhoto() != null){
//            userProfile.setAvatarUrl(   ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/file/").path(user.getPhoto().getId().toString()).toUriString());
//        }
//        return userProfile;
//    }
//}
