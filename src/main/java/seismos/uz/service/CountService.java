package seismos.uz.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import seismos.uz.entity.Disertation;
import seismos.uz.entity.TakenBooks;
import seismos.uz.entity.TakenDisertations;
import seismos.uz.payload.ResGetBooks;
import seismos.uz.repository.*;

import java.util.*;

@Service
public class CountService {

    private BookRepository bookRepository;
    private TakenBooksRepository takenBooksRepository;
    private DisertationRepository disertationRepository;
    private TakenDisertationsRepository takenDisertationsRepository;
    private AvtoReferatRepository avtoReferatRepository;
    private TakenAvtoReferatsRepository  takenAvtoReferatsRepository;
    private JournalRepository journalRepository;
    private TakenJournalsRepository takenJournalsRepository;
    private ReportRepository reportRepository;
    private TakenReportsRepository takenReportsRepository;

    @Autowired
    public CountService(BookRepository bookRepository, TakenBooksRepository takenBooksRepository, DisertationRepository disertationRepository, TakenDisertationsRepository takenDisertationsRepository, AvtoReferatRepository avtoReferatRepository, TakenAvtoReferatsRepository takenAvtoReferatsRepository, JournalRepository journalRepository, TakenJournalsRepository takenJournalsRepository, ReportRepository reportRepository, TakenReportsRepository takenReportsRepository, Set<String> takenBookNameList) {
        this.bookRepository = bookRepository;
        this.takenBooksRepository = takenBooksRepository;
        this.disertationRepository = disertationRepository;
        this.takenDisertationsRepository = takenDisertationsRepository;
        this.avtoReferatRepository = avtoReferatRepository;
        this.takenAvtoReferatsRepository = takenAvtoReferatsRepository;
        this.journalRepository = journalRepository;
        this.takenJournalsRepository = takenJournalsRepository;
        this.reportRepository = reportRepository;
        this.takenReportsRepository = takenReportsRepository;
        this.takenBookNameList = takenBookNameList;
    }


    Set<String> takenBookNameList = new HashSet<>();

    public List<ResGetBooks> getBooks(){
        List<ResGetBooks> getTakenBooksList = new ArrayList<>();
        takenBookNameList.clear();

        for (TakenBooks book: takenBooksRepository.findAll()) {
            takenBookNameList.add(book.getSoureName());
        }

        for (String bookName:takenBookNameList) {
             ResGetBooks getBooks = new ResGetBooks();
             long count = takenBooksRepository.countBySoureName(bookName);
             getBooks.setName(bookName);
             getBooks.setQuantity(count);
             getTakenBooksList.add(getBooks);
        }
        System.out.println(getTakenBooksList);
        return getTakenBooksList;
    }

}