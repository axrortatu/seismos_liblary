package seismos.uz.service;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import seismos.uz.entity.*;
import seismos.uz.repository.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.UUID;


@Service
public class ExcelGenerator {
    private final BookRepository bookRepository;
    private final DisertationRepository disertationRepository;
    private final AvtoReferatRepository avtoReferatRepository;
    private final ReportRepository reportRepository;
    private final JournalRepository journalRepository;

    @Autowired
    public ExcelGenerator(BookRepository bookRepository, DisertationRepository disertationRepository, AvtoReferatRepository avtoReferatRepository, ReportRepository reportRepository, JournalRepository journalRepository) {
        this.bookRepository = bookRepository;
        this.disertationRepository = disertationRepository;
        this.avtoReferatRepository = avtoReferatRepository;
        this.reportRepository = reportRepository;
        this.journalRepository = journalRepository;
    }

    public  ByteArrayInputStream customersToExcel(int beginDate, int endDate, String check) throws IOException {
        String[] COLUMNs = { "Nomi", "Kutubxona bazasidagi soni", "Id_number", "Yaratilgan Vaqti"};
        try(
                Workbook workbook = new XSSFWorkbook();
                ByteArrayOutputStream out = new ByteArrayOutputStream();
        ){
            CreationHelper createHelper = workbook.getCreationHelper();
            Sheet sheet = workbook.createSheet("books");

            Font headerFont = workbook.createFont();
            headerFont.setBold(true);
            headerFont.setColor(IndexedColors.BLUE.getIndex());


            CellStyle headerCellStyle = workbook.createCellStyle();
            headerCellStyle.setFont(headerFont);

            Row headerRow = sheet.createRow(0);

            for (int col = 0; col < COLUMNs.length; col++) {
                Cell cell = headerRow.createCell(col);
                cell.setCellValue(COLUMNs[col]);
                cell.setCellStyle(headerCellStyle);
            }

            int rowIdx = 1;

            if (check.equals("book")) {
                for (Book book : bookRepository.findAll()) {
                    Row row = sheet.createRow(rowIdx++);
                    if ((book.getCreated_date() >= beginDate && book.getCreated_date() <= endDate)) {
                        row.createCell(0).setCellValue(book.getName());
                        row.createCell(1).setCellValue(book.getQuantity());
                        row.createCell(2).setCellValue(book.getId_number());
                        row.createCell(3).setCellValue(book.getCreated_date());
                    }
                }
            }else if (check.equals("journal")){

                for (Journal book : journalRepository.findAll()) {
                    Row row = sheet.createRow(rowIdx++);
                    if ((book.getCreated_date() >= beginDate && book.getCreated_date() <= endDate)) {
                        row.createCell(0).setCellValue(book.getName());
                        row.createCell(1).setCellValue(book.getQuantity());
                        row.createCell(2).setCellValue(book.getId_number());
                        row.createCell(3).setCellValue(book.getCreated_date());
                    }
                }
            }else if (check.equals("avtoreferat")){

                for (AvtoReferat book : avtoReferatRepository.findAll()) {
                    Row row = sheet.createRow(rowIdx++);
                    if ((book.getCreated_date() >= beginDate && book.getCreated_date() <= endDate)) {
                        row.createCell(0).setCellValue(book.getName());
                        row.createCell(1).setCellValue(book.getQuantity());
                        row.createCell(2).setCellValue(book.getId_number());
                        row.createCell(3).setCellValue(book.getCreated_date());
                    }
                }
            }else if (check.equals("report")){

                for (Report book : reportRepository.findAll()) {
                    Row row = sheet.createRow(rowIdx++);
                    if ((book.getCreated_date() >= beginDate && book.getCreated_date() <= endDate)) {
                        row.createCell(0).setCellValue(book.getName());
                        row.createCell(1).setCellValue(book.getQuantity());
                        row.createCell(2).setCellValue(book.getId_number());
                        row.createCell(3).setCellValue(book.getCreated_date());
                    }
                }
            }else if (check.equals("disertation")){

                for (Disertation book : disertationRepository.findAll()) {
                    Row row = sheet.createRow(rowIdx++);
                    if ((book.getCreated_date() >= beginDate && book.getCreated_date() <= endDate)) {
                        row.createCell(0).setCellValue(book.getName());
                        row.createCell(1).setCellValue(book.getQuantity());
                        row.createCell(2).setCellValue(book.getId_number());
                        row.createCell(3).setCellValue(book.getCreated_date());
                    }
                }
            }
            workbook.write(out);
            return new ByteArrayInputStream(out.toByteArray());
        }

    }
}
