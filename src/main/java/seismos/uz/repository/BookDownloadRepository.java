package seismos.uz.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import seismos.uz.entity.BookDownload;

import java.util.UUID;

@RepositoryRestResource(path = "downloadBook")
public interface BookDownloadRepository extends JpaRepository<BookDownload, UUID> {
}
