package seismos.uz.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import seismos.uz.entity.Book;
import seismos.uz.entity.TakenDisertations;

import java.util.UUID;


@RepositoryRestResource(path = "taken_dissertations" , collectionResourceRel = "sources")
public interface TakenDisertationsRepository extends JpaRepository<TakenDisertations, UUID> {
    long countBySoureName(String sourceName);

    @Query("select b from TakenDisertations b where name like %?1%")
    Page<TakenDisertations> findByFullName(String name, Pageable pageable);

//    @Query("select b from TakenDisertationsRepository b where name like %?1%")
//    Page<TakenDisertationsRepository> findBySoureNameOrFirstNameOrLastName(String s, Pageable pageable);

}
