package seismos.uz.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import seismos.uz.entity.Attachment;
import seismos.uz.entity.AttachmentContent;

import java.util.Optional;
import java.util.UUID;

public interface AttachmentContentRepository extends JpaRepository<AttachmentContent, UUID>{
       Optional<AttachmentContent> findByAttachment(Attachment attachment);
}