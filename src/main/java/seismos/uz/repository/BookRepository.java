package seismos.uz.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.security.access.prepost.PreAuthorize;
import seismos.uz.entity.Book;

import java.util.List;
import java.util.UUID;

@RepositoryRestResource(path = "/book", collectionResourceRel="book_list")
public interface BookRepository extends JpaRepository<Book, UUID> {

    @PreAuthorize(value = "hasRole('ROLE_ADMIN')")
    @Override
    <S extends Book> S save(S s);

    @PreAuthorize(value = "hasRole('ROLE_ADMIN')")
    @Override
    void deleteById(UUID uuid);

    @Query("select b from Book b where name like %?1%")
    Page<Book> findByNameOrCreatedDate(String name, Pageable pageable);

}
