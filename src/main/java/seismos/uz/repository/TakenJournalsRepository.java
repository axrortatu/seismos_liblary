package seismos.uz.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import seismos.uz.entity.Book;
import seismos.uz.entity.TakenJournals;

import java.util.UUID;

@RepositoryRestResource(path = "taken_journals" , collectionResourceRel = "sources")
public interface TakenJournalsRepository extends JpaRepository<TakenJournals, UUID> {
    long countBySoureName(String sourceName);

    @Query("select b from TakenJournals b where name like %?1%")
    Page<TakenJournals> findByFullName(String name, Pageable pageable);
//    @Query("select b from TakenJournalsRepository b where name like %?1%")
//    Page<TakenJournalsRepository> findBySoureNameOrFirstNameOrLastName(String s, Pageable pageable);
}
