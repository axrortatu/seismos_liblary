package seismos.uz.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import seismos.uz.entity.Attachment;

import java.util.UUID;

public interface AttachmenntRepository extends JpaRepository<Attachment, UUID> {

}
