package seismos.uz.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import seismos.uz.entity.BookDownload;
import seismos.uz.entity.Disertation;
import seismos.uz.entity.DisertationDownload;

import java.util.UUID;

@RepositoryRestResource(path = "downloadDisertation")
public interface DisertationDownloadRepository extends JpaRepository<DisertationDownload, UUID> {
}
