package seismos.uz.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import seismos.uz.entity.BookDownload;
import seismos.uz.entity.ReportDownLoad;

import java.util.UUID;

@RepositoryRestResource(path = "downloadReport")
public interface ReportDownloadRepository extends JpaRepository<ReportDownLoad, UUID> {
}
