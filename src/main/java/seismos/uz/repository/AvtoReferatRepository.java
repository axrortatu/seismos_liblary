package seismos.uz.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.security.access.prepost.PreAuthorize;
import seismos.uz.entity.AvtoReferat;

import java.util.UUID;
@RepositoryRestResource(path = "/avtoreferat", collectionResourceRel="avtoreferat_list")
public interface AvtoReferatRepository extends JpaRepository<AvtoReferat, UUID> {


    @PreAuthorize(value = "hasRole('ROLE_ADMIN')")
    @Override
    <S extends AvtoReferat> S save(S s);

    @PreAuthorize(value = "hasRole('ROLE_ADMIN')")
    @Override
    void deleteById(UUID uuid);

    @Query("select b from AvtoReferat b where name like %?1%")
    Page<AvtoReferat> findByName(String s, Pageable pageable);
}
