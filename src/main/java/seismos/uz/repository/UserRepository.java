package seismos.uz.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import seismos.uz.entity.Role;
import seismos.uz.entity.User;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User,UUID> {
     Optional<User> findByUsername(String name);
}
