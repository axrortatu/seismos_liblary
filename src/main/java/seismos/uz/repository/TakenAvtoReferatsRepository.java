package seismos.uz.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import seismos.uz.entity.Book;
import seismos.uz.entity.Report;
import seismos.uz.entity.TakenAvtoReferats;

import java.util.UUID;


@RepositoryRestResource(path = "taken_avtoreferats" , collectionResourceRel = "sources")
public interface TakenAvtoReferatsRepository extends JpaRepository<TakenAvtoReferats, UUID> {
     long countBySoureName(String sourceName);

     @Query("select b from TakenAvtoReferats b where name like %?1%")
     Page<TakenAvtoReferats> findByFullName(String name, Pageable pageable);

//     @Query("select b from TakenAvtoReferatsRepository b where name like %?1%")
//     Page<TakenAvtoReferatsRepository> findBySoureNameOrFirstNameOrLastName(String s, Pageable pageable);
}
