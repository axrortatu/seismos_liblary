package seismos.uz.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import seismos.uz.entity.Role;
import seismos.uz.enums.RoleName;

import java.util.List;

public interface RoleRepository extends JpaRepository<Role, Integer> {
    List<Role> findAllByRoleName(RoleName roleName);
}
