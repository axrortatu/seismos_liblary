package seismos.uz.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.security.access.prepost.PreAuthorize;
import seismos.uz.entity.Book;
import seismos.uz.entity.Journal;
import seismos.uz.entity.Report;

import java.util.UUID;

@RepositoryRestResource(path = "/report", collectionResourceRel="report_list")
public interface ReportRepository extends JpaRepository<Report, UUID> {
    @PreAuthorize(value = "hasRole('ROLE_ADMIN')")
    @Override
    <S extends Report> S save(S s);

    @PreAuthorize(value = "hasRole('ROLE_ADMIN')")
    @Override
    void deleteById(UUID uuid);

    @Query("select b from Report b where name like %?1%")
    Page<Report> findByName(String s, Pageable pageable);
}
