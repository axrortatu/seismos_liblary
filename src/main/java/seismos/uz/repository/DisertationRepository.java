package seismos.uz.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.security.access.prepost.PreAuthorize;
import seismos.uz.entity.Book;
import seismos.uz.entity.Disertation;

import java.util.UUID;
@RepositoryRestResource(path = "/disertation", collectionResourceRel="disertation_list")
public interface DisertationRepository extends JpaRepository<Disertation, UUID> {

    @PreAuthorize(value = "hasRole('ROLE_ADMIN')")
    @Override
    <S extends Disertation> S save(S s);

    @PreAuthorize(value = "hasRole('ROLE_ADMIN')")
    @Override
    void deleteById(UUID uuid);

    @Query("select b from Disertation b where name like %?1%")
    Page<Disertation> findByName(String s, Pageable pageable);
}
