package seismos.uz.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import seismos.uz.entity.Book;
import seismos.uz.entity.TakenBooks;


import java.util.Optional;
import java.util.UUID;

@RepositoryRestResource(path = "taken_books" , collectionResourceRel = "sources")
public interface TakenBooksRepository extends JpaRepository<TakenBooks, UUID> {

    long countBySoureName(String sourceName);

    @Query("select b from TakenBooks b where name like %?1%")
    Page<TakenBooks> findByFullName(String name, Pageable pageable);

//    @Query("select b from TakenBooksRepository b where name like %?1%")
//    Page<TakenBooksRepository> findBySoureNameOrFirstNameOrLastName(String s, Pageable pageable);

}
