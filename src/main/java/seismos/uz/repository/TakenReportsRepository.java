package seismos.uz.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import seismos.uz.entity.Book;
import seismos.uz.entity.TakenReports;

import java.util.UUID;

@RepositoryRestResource(path = "taken_reports", collectionResourceRel = "sources")
public interface TakenReportsRepository extends JpaRepository<TakenReports, UUID> {
      long countBySoureName(String sourceName);

      @Query("select b from TakenReports b where name like %?1%")
      Page<TakenReports> findByFullName(String name, Pageable pageable);

//      @Query("select b from TakenReportsRepository b where name like %?1%")
//      Page<TakenReportsRepository> findBySoureNameOrFirstNameOrLastName(String s, Pageable pageable);
}
