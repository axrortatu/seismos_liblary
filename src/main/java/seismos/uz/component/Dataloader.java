//package seismos.uz.component;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.boot.CommandLineRunner;
//import org.springframework.security.crypto.password.PasswordEncoder;
//import org.springframework.stereotype.Component;
//import seismos.uz.entity.User;
//import seismos.uz.enums.RoleName;
//import seismos.uz.repository.RoleRepository;
//import seismos.uz.repository.UserRepository;
//
//
//@Component
//public class Dataloader implements CommandLineRunner {
//
//    @Value("${spring.datasource.initialization-mode}")
//    private String initialMode;
//
//    @Autowired
//    UserRepository userRepository;
//
//    @Autowired
//    RoleRepository roleRepository;
//
//    @Autowired
//    PasswordEncoder passwordEncoder;
//
//    @Override
//    public void run(String... args) throws Exception {
//
//        if (initialMode.equals("always")){
//            userRepository.save( new User("Axror","Mamasodirov","web-developer", passwordEncoder.encode("seismos"), "+998949418545", roleRepository.findAllByRoleName(RoleName.ROLE_ADMIN)));
//            userRepository.save( new User("Sobir","Sadirov","user", passwordEncoder.encode("123"), "+998945145626",roleRepository.findAllByRoleName(RoleName.ROLE_USER)));
//        }
//
//    }
//
//}
