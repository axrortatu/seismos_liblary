package seismos.uz.enums;

public enum RoleName {
    ROLE_USER,
    ROLE_ADMIN
}
